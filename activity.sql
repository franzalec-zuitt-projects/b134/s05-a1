

-- 1.
SELECT customerName FROM customers WHERE country = "Philippines";

-- 2.
SELECT contactLastName, contactFirstName FROM customers WHERE customerName = "La Rochelle Gifts";

-- 3.
SELECT productName, MSRP FROM products WHERE productName = "The Titanic";

-- 4.
SELECT firstName, lastName FROM employees WHERE email = "jfirrelli@classicmodelcars.com";

-- 5.
SELECT customerName FROM customers WHERE state IS NULL;

-- 6.
SELECT firstName, lastName, email FROM employees WHERE lastName = "Patterson" AND firstName = "Steve";

-- 7.
SELECT customerName, country, creditLimit FROM customers WHERE country != "USA" AND creditLimit > 3000;

-- 8.
SELECT customerName FROM customers WHERE customerName NOT LIKE "%a%";

-- 9.
SELECT customerNumber FROM orders WHERE  comments LIKE "%DHL%";

-- 10.
SELECT productLine FROM productlines WHERE textDescription LIKE "%state of the art%";

-- 11.
SELECT DISTINCT country FROM customers;

-- 12.
SELECT DISTINCT status FROM orders;

-- 13.
SELECT customerName, country FROM customers WHERE  country = "USA" OR country = "France" OR country = "Canada";

-- 14.
SELECT employees.firstName, employees.lastName, offices.city
FROM  employees JOIN offices
ON employees.officeCode = offices.officeCode
WHERE  offices.city= "Tokyo";

-- 15.
SELECT customers.customerName
FROM customers JOIN employees 
ON customers.salesRepEmployeeNumber = employees.employeeNumber 
WHERE employees.firstName = "Leslie"
AND
employees.lastName = "Thompson";

-- 16.
SELECT products.productName, customers.customerName
FROM orderdetails JOIN orders, products, customers
ON orderdetails.productCode = products.productCode,
orderdetails.orderNumber = orders.orderNumber,
orders.customerNumber = customers.customerName
WHERE customers.customerName = "Baane Mini Imports"; 

-- 17.
SELECT  FROM  WHERE  = "";

-- 18.
SELECT lastName, firstName FROM employees WHERE reportsTo = 1143;

-- 19.
SELECT productName, MSRP FROM products WHERE MSRP = 214.30;

-- 20.
SELECT COUNT(*) FROM customers WHERE country = "UK";

-- 21.
SELECT COUNT(*) FROM products WHERE DISTINCT productLine;

-- 22.
SELECT COUNT(*) FROM customers WHERE salesRepEmployeeNumber IS NOT NULL;

-- 23.
SELECT productName, quantityInStock FROM products WHERE productLine = "Planes" AND quantityInStock < 1000;
